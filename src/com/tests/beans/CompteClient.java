package com.tests.beans;

public class CompteClient {
private int numero;
private int idClient;
private String dateCreation;
private double chiffreAffaire;
public CompteClient()
{
	
}
public CompteClient(int numero, int idClient, String dateCreation, double chiffreAffaire) {
	super();
	this.numero = numero;
	this.idClient = idClient;
	this.dateCreation = dateCreation;
	this.chiffreAffaire = chiffreAffaire;
}
public CompteClient(int numero)
{
	this.numero=numero;
}
public int getNumero() {
	return numero;
}
public void setNumero(int numero) {
	this.numero = numero;
}
public int getIdClient() {
	return idClient;
}
public void setIdClient(int idClient) {
	this.idClient = idClient;
}
public String getDateCreation() {
	return dateCreation;
}
public void setDateCreation(String dateCreation) {
	this.dateCreation = dateCreation;
}
public double getChiffreAffaire() {
	return chiffreAffaire;
}
public void setChiffreAffaire(double chiffreAffaire) {
	this.chiffreAffaire = chiffreAffaire;
}

}
