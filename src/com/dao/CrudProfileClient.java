package com.dao;
import java.sql.*;
import java.util.*;
import com.tests.beans.*;

public class CrudProfileClient implements intProfileClientDao {
	private Connection maconnexion =Connexion.getConnection();
	public CrudProfileClient() {
	}
	@Override
	public List<ProfilClient> getAllComptes() {
	List<ProfilClient> l = new ArrayList<ProfilClient>();
	ProfilClient c;
	try {
	PreparedStatement statement=maconnexion.prepareStatement("Select idProp,nom,prenom,societe,ville,adresse from profilclient");
	ResultSet res=statement.executeQuery();
	while (res.next())
// parcourir tous les comptes
	{
	c= new ProfilClient();
	c.setIdProp(res.getInt(1));
	c.setNom(res.getString(2));
	c.setPrenom(res.getString(3));
	c.setSociete(res.getString(4));
	c.setVille(res.getString(5));
	c.setAdresse(res.getString(6));
	l.add(c);
	}
	} catch(SQLException e1) {
	e1.printStackTrace();
	l=null;
	}
	return l;
	}
	@Override
	public void addProfil(ProfilClient c) {
	try {
	PreparedStatement
	statement=maconnexion.prepareStatement("Insert into profilclient(nom,prenom,societe,ville,adresse) values (?,?,?,?,?)");
	statement.setString(1,c.getNom());
	statement.setString(2,c.getPrenom());
	statement.setString(3,c.getSociete());
	statement.setString(4,c.getVille());
	statement.setString(5,c.getAdresse());
	statement.executeUpdate();
	statement.close();
	System.out.println("succes ajout");
	} catch (SQLException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
	}
	}
	@Override
	public CompteClient findCompte(int idProp) throws SQLException {
	// TODO Auto-generated method stub
		ProfilClient pc =null;
		String req = "SELECT * FROM  profilclient where idProp=?";
		PreparedStatement statement = maconnexion.prepareStatement(req);
		statement.setInt(1,idProp);
		ResultSet  rs = statement.executeQuery();
		if (rs.next())
		{
			String nom = rs.getString("nom");
			String prenom = rs.getString("prenom");
			String societe = rs.getString("societe");
			String ville = rs.getString("ville");
			String adresse= rs.getString("adresse");
			pc = new ProfilClient(idProp,nom,prenom,societe,ville,adresse);
			
		}
		
	return null;
	}
	@Override
	public boolean deleteCompte(int idProp) throws SQLException{
	// TODO Auto-generated method stub
		String req = "DELETE FROM profilclient WHERE idProp=?";
		PreparedStatement statement = maconnexion.prepareStatement(req);
		statement.setInt(1,idProp);
		boolean deleted = statement.executeUpdate() > 0;
	return deleted;
	}
	}
