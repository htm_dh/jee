package com.tests.servlets;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CrudProfileClient;
import com.dao.intProfileClientDao;
import com.tests.beans.ProfilClient;

/**
 * Servlet implementation class AjoutProfilClient
 */
@WebServlet("/AjoutProfilClient")
public class AjoutProfilClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private intProfileClientDao profilClientsDao= new CrudProfileClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutProfilClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setAttribute("profilClient",profilClientsDao.getAllComptes());
		  this.getServletContext().getRequestDispatcher("/WEB-INF/ProfileClient.jsp").forward(request,response); 
		  }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProfilClient profilClient = new ProfilClient();
		profilClient.setNom(request.getParameter("nom"));
		profilClient.setPrenom(request.getParameter("prenom"));
		profilClient.setVille(request.getParameter("ville"));
		profilClient.setSociete(request.getParameter("societe"));
		profilClient.setAdresse(request.getParameter("adresse")); 
		profilClientsDao.addProfil(profilClient);
		request.setAttribute("profileclient", profilClientsDao.getAllComptes());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ProfileClient.jsp").forward(request,response); 
		
	}

}
