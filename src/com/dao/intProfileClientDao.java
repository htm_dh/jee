package com.dao;
import com.tests.beans.*;

import java.sql.SQLException;
import java.util.List;
import com.tests.beans.CompteClient;

public interface intProfileClientDao {
	public List<ProfilClient> getAllComptes();
	public void addProfil(ProfilClient c);
	public CompteClient findCompte(int idProp) throws SQLException ;
	public boolean deleteCompte(int idProp) throws SQLException;

}
