package com.dao;
import com.tests.beans.*;

import java.sql.SQLException;
import java.util.List;
public interface intCompteClientDao {
	public List<CompteClient> getAllComptes();
	public void addCompte(CompteClient c);
	public CompteClient findCompte(int numero) throws SQLException ;
	public boolean deleteCompte(int id) throws SQLException;

}
