package com.tests.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CrudProfileClient;
import com.dao.intProfileClientDao;

/**
 * Servlet implementation class RechercheProfil
 */
@WebServlet("/RechercheProfil")
public class RechercheProfil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private intProfileClientDao profilDao= new CrudProfileClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RechercheProfil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		int id =Integer.parseInt(request.getParameter("id"));
		 try {
			 request.setAttribute("profil",profilDao.findCompte(id));
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeProfileClient.jsp");
				dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
