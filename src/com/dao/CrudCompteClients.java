package com.dao;
import java.sql.*;
import java.util.*;
import com.tests.beans.*;

public class CrudCompteClients implements intCompteClientDao {
	private Connection maconnexion =Connexion.getConnection();
	public CrudCompteClients() {
	}
	@Override
	public List<CompteClient> getAllComptes() {
	List<CompteClient> l = new ArrayList<CompteClient>();
	CompteClient c;
	try {
	PreparedStatement statement=maconnexion.prepareStatement("Select numero,idClient,dateCreation, chiffreAffaire from compteclient");
	ResultSet res=statement.executeQuery();
	while (res.next())
// parcourir tous les comptes
	{
	c= new CompteClient();
	c.setNumero(res.getInt(1));
	c.setIdClient(res.getInt(2));
	c.setDateCreation(res.getString(3));
	c.setChiffreAffaire(res.getDouble(4));
	l.add(c);
	}
	} catch(SQLException e1) {
	e1.printStackTrace();
	l=null;
	}
	return l;
	}
	@Override
	public void addCompte(CompteClient c) {
	try {
	PreparedStatement
	statement=maconnexion.prepareStatement("Insert into compteclient(idClient,dateCreation,chiffreAffaire) values (?,?,?)");
	statement.setInt(1,c.getIdClient());
	statement.setString(2,c.getDateCreation());
	statement.setDouble(3,c.getChiffreAffaire());
	statement.executeUpdate();
	statement.close();
	System.out.println("succes ajout");
	} catch (SQLException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
	}
	}
	@Override
	public CompteClient findCompte(int numero) throws SQLException {
	// TODO Auto-generated method stub
		CompteClient c =null;
		String req = "SELECT * FROM  compteclient where numero=?";
		PreparedStatement statement = maconnexion.prepareStatement(req);
		statement.setInt(1,numero);
		ResultSet  rs = statement.executeQuery();
		if (rs.next())
		{
			int id = rs.getInt("idClient");
			String date= rs.getString("dateCreation");
			double chiffre =rs.getDouble("chiffreAffaire");
			c = new CompteClient(numero,id,date,chiffre);
			
		}
		
		
	return c;
	}
	@Override
	public boolean deleteCompte(int id)throws SQLException{
	String req = "DELETE FROM compteclient WHERE numero=?";
		PreparedStatement statement = maconnexion.prepareStatement(req);
		statement.setInt(1,id);
		boolean deleted = statement.executeUpdate() > 0;
	return deleted;
	}
	}
