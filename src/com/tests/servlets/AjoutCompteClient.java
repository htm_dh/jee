package com.tests.servlets;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CrudCompteClients;
import com.dao.intCompteClientDao;
import com.tests.beans.CompteClient;

/**
 * Servlet implementation class AjoutCompteClient
 */
@WebServlet("/AjoutCompteClient")
public class AjoutCompteClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private intCompteClientDao compteClientsDao= new CrudCompteClients();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AjoutCompteClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  request.setAttribute("compteClients",compteClientsDao.getAllComptes());
		  this.getServletContext().getRequestDispatcher("/WEB-INF/CompteClient.jsp").forward(request,response); 
		  }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CompteClient compteClient = new CompteClient();
		compteClient.setIdClient(Integer.parseInt(request.getParameter("idClient")));  
		compteClient.setDateCreation(request.getParameter("dateCreation")); 
		compteClient.setChiffreAffaire(Double.parseDouble(request.getParameter("chiffreAffaire"))); 
		compteClientsDao.addCompte(compteClient);
		request.setAttribute("compteClients", compteClientsDao.getAllComptes());
		this.getServletContext().getRequestDispatcher("/WEB-INF/CompteClient.jsp").forward(request,response); 
		
	}

}
