<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<div class="container">
	<div class="row">
	<div class="col-md-12">
	<h4><b><center>Liste Client</center></b></h4>
	<br>
	<br>
	<br>
	<br>
<form action="RechercheCompte" method="post">
<label>Chercher</label>
<input type="text" name="id" value="${param.id}" />
<button type="submit">Chercher</button>
</form>
<br>
<br>
<br>
<br>

<div class="table-responsive">
<table id="mytable" class="table table-bordred table-striped">
<tr>
<th>Numero</th>
<th> Id Client</th>
<th> Date Creation </th>
<th> Chiffre Affaire</th>
<th> Action</th>
</tr>
<c:forEach var="compteClient" items="${ compteClients }"> 
<tr>
<td><c:out value="${ compteClient.numero }" /></td>
<td><c:out value="${ compteClient.idClient }" /> </td>
<td><c:out value="${ compteClient.dateCreation }" /></td>
<td><c:out value="${ compteClient.chiffreAffaire }" /> </td>
<td>
<a href="delete?id=${compteClient.numero}" onClick="return confirm('Voulez vous vraiment supprimer ?')">Delete</a>
</td>
</tr>
</c:forEach>
</table>
</div>
<p>${compte.numero }</p>
<p>${compte.dateCreation }</p>
<p>${compte.chiffreAffaire}</p>
</body>
</html>