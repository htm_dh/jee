	package com.tests.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.CrudCompteClients;
import com.dao.intCompteClientDao;

/**
 * Servlet implementation class SupprimerClient
 */
@WebServlet("/delete")
public class SupprimerClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private intCompteClientDao compteClientsDao= new CrudCompteClients();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SupprimerClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		int id = Integer.parseInt(request.getParameter("id")); 
		try {
			compteClientsDao.deleteCompte(id);
			request.setAttribute("compteClients",compteClientsDao.getAllComptes());
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeCompteClient.jsp");
			dispatcher.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	}

}
