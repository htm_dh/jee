package com.tests.beans;

public class FullCompte {
private int numero;
private int idProp;
private String nom;
private String prenom;
private String societe;
private double chiffreAffaires;
public FullCompte(int numero, int idProp, String nom, String prenom, String societe, double chiffreAffaires) {
	super();
	this.numero = numero;
	this.idProp = idProp;
	this.nom = nom;
	this.prenom = prenom;
	this.societe = societe;
	this.chiffreAffaires = chiffreAffaires;
}
public int getNumero() {
	return numero;
}
public void setNumero(int numero) {
	this.numero = numero;
}
public int getIdProp() {
	return idProp;
}
public void setIdProp(int idProp) {
	this.idProp = idProp;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}
public String getSociete() {
	return societe;
}
public void setSociete(String societe) {
	this.societe = societe;
}	
public double getChiffreAffaires() {
	return chiffreAffaires;
}
public void setChiffreAffaires(double chiffreAffaires) {
	this.chiffreAffaires = chiffreAffaires;
}


}
