package com.tests.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import com.dao.CrudCompteClients;
import com.dao.CrudProfileClient;
import com.dao.intCompteClientDao;
import com.dao.intProfileClientDao;
import com.tests.beans.CompteClient;

/**
 * Servlet implementation class ListeProfilClient
 */
@WebServlet("/ListeProfilClient")
public class ListeProfilClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private intProfileClientDao profilDao= new CrudProfileClient();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeProfilClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		if (action.equals("/delete"))
		{
			int id = Integer.parseInt(request.getParameter("idProp")); 
			try {
				profilDao.deleteCompte(id);
				request.setAttribute("profilClient",profilDao.getAllComptes());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			else if (action.equals("/fetch"))
			{
				int id =Integer.parseInt(request.getParameter("id"));
				CompteClient cmpt = new CompteClient();
				cmpt.setNumero(id);
				request.setAttribute("profilClient",profilDao.getAllComptes());
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeProfilClient.jsp");
				dispatcher.forward(request, response);
			}
			else
			{
				response.sendError(Response.SC_NOT_FOUND);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
