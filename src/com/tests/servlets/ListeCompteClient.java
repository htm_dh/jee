package com.tests.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Response;

import com.dao.CrudCompteClients;
import com.dao.intCompteClientDao;
import com.tests.beans.CompteClient;

/**
 * Servlet implementation class ListeCompteClient
 */
@WebServlet("/ListeCompteClient")
public class ListeCompteClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private intCompteClientDao compteClientsDao= new CrudCompteClients();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListeCompteClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		System.out.println(action);
		if (action.equals("/delete"))
		{
			int id = Integer.parseInt(request.getParameter("numero")); 
			try {
				compteClientsDao.deleteCompte(id);
				request.setAttribute("compteClients",compteClientsDao.getAllComptes());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			else if (action.equals("/fetch"))
			{
				int id =Integer.parseInt(request.getParameter("id"));
				//cmpt.setNumero(id);
				 try {
					 request.setAttribute("compte",compteClientsDao.findCompte(id));
					 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeCompteClient.jsp");
						dispatcher.forward(request, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			else
			{
				request.setAttribute("compteClients",compteClientsDao.getAllComptes());
				 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeCompteClient.jsp");
					dispatcher.forward(request, response);
			}
			
		}
		
		/* try {
	            switch (action) {
	            case "/delete":
	            	deleteuser(request, response);
	                break;
	            default:
	             listcompt(request, response);
	                break;
	            }
	        } catch (SQLException ex) {
	            throw new ServletException(ex);
	        }*/

	/*private void listcompt(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        request.setAttribute("compteClients",compteClientsDao.getAllComptes());
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/ListeCompteClient.jsp");
        dispatcher.forward(request, response);
    }*/

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
